<?php

/**
 * Implementation of hook_form_alter().
 */
function preset_widget_options_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == '_content_admin_field' && $form['field_type']['#value'] == 'text') {
    $type = content_types($form['type_name']['#value']);
    $field = $type['fields'][$form['field_name']['#value']];
    $options = preset_widget_options_options();
    $form['field']['advanced_options']['#title'] = t('Advanced options');
    $form['field']['advanced_options']['allowed_values_php']['#title'] = t('PHP code');
    if (isset($field['preset_widget_options']) && !empty($field['preset_widget_options'])) {
      $form['field']['advanced_options']['allowed_values_php']['#disabled'] = TRUE;
      $form['field']['advanced_options']['allowed_values_php']['#description'] .= ' ' . t('PHP code is disabled because an option handler is selected below. To reenable PHP code, deselect the option handler.');
    }
    $form['field']['advanced_options']['preset_widget_options'] = array(
      '#type' => 'select',
      '#title' => t('Options handler'),
      '#description' => t('Alternately, select a preset handler to provide options for this field. If set, this handler will overwrite the PHP code above.'),
      '#options' => $options,
      '#default_value' => $field['preset_widget_options'],
    );
    $form['#validate'][] = 'preset_widget_options_content_admin_field_validate';
    $form['#submit'][] = 'preset_widget_options_content_admin_field_submit';
  }
}

/**
 * Validate handler for _content_admin_field form; conditionally set allowed_values_php
 * value.
 */
function preset_widget_options_content_admin_field_validate($form_id, $form_values, $form) {
  if (!empty($form_values['preset_widget_options'])) {
    form_set_value($form['field']['advanced_options']['allowed_values_php'], _preset_widget_options_php($form_values['preset_widget_options']));
  }
}

/**
 * Submit handler for _content_admin_field form; register preset_widget_options value
 * to field settings.
 */
function preset_widget_options_content_admin_field_submit($form_id, $form_values) {
  $field_settings = unserialize(db_result(db_query("SELECT global_settings FROM {node_field} WHERE field_name = '%s'", $form_values['field_name'])));
  $field_settings['preset_widget_options'] = $form_values['preset_widget_options'];
  db_query("UPDATE {node_field} SET global_settings = '%s' WHERE field_name = '%s'", serialize($field_settings), $form_values['field_name']);
  // Refresh the cached data.
  content_clear_type_cache();
}

/**
 * Generate a PHP snippet.
 */
function _preset_widget_options_php($key) {
  list($module, $type) = explode('|', $key);
  $php = "return preset_widget_options_process('$module', '$type');";
  return $php;
}

/**
 * Provide value options for a field.
 */
function preset_widget_options_process($module, $type) {
  $options = array();
  $data = preset_widget_options_data();
  if (isset($data[$module . '|' . $type]) && isset($data[$module . '|' . $type]['#options'])) {
    foreach ($data[$module . '|' . $type]['#options'] as $option => $option_values) {
      $options[$option] = $option_values['#title'];
    }
  }
  return $options;
}

/**
 * Fetch widget options data from all implementing modules.
 */
function preset_widget_options_data($refresh = FALSE) {
  static $data = array();
  if (empty($data) || $refresh) {
    foreach(module_implements('preset_widget_options') as $module) {
      $types = module_invoke($module, 'preset_widget_options');
      // Preprocess the array keys for ease of reference.
      foreach ($types as $type => $type_values) {
        $data[$module . '|' . $type] = $type_values;
      }
    }
  }
  return $data;
}

/**
 * Provide select options of available preset widget options for the field admin form.
 */
function preset_widget_options_options() {
  $options = array(
    0 => t('None'),
  );
  $data = preset_widget_options_data();
  foreach ($data as $module_type => $type_values) {
    $options[$module_type] = $type_values['#title'];
  }
  return $options;
}

/**
 * Implementation of hook_field_formatter_info().
 */
function preset_widget_options_field_formatter_info() {
  $info = array();
  $data = preset_widget_options_data();
  foreach ($data as $module_type => $type_values) {
    $info['preset|' . $module_type] = array(
      'label' => $type_values['#title'],
      'field types' => array('text'),
    );
  }
  return $info;
}

/**
 * Implementation of hook_field_formatter().
 */
function preset_widget_options_field_formatter($field, $item, $formatter, $node) {
  if (!isset($item['value'])) {
    return '';
  }
  if ($allowed_values = text_allowed_values($field)) {
    if (isset($allowed_values[$item['value']])) {
      list($preset, $module, $type) = explode('|', $formatter);
      $data = preset_widget_options_data();
      $option = $data[$module . '|' . $type]['#options'][$item['value']];
      // Use a type-specific theme function if present.
      $theme_function = function_exists('theme_' . $module . '_' . $type . '_field_value') ? $module . '_' . $type  . '_field_value' : 'preset_widget_options_field_value';
      return theme($theme_function, $option);
    }
  }

  return '';
}

/**
 * Default theme handler for widget option fields.
 */
function theme_preset_widget_options_field_value($option) {
  return '<p>' . $option['#title'] . '</p>';
}

